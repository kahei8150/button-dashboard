## Application Description

The application has two main pages:

1. **Client Page (http://localhost:8080/client):** This page displays two buttons - one orange and one blue.

2. **Dashboard Page (http://localhost:8080/dashboard):** This page displays a dashboard that updates live with the number of clicks from the client page. The updates happen without polling the database. After 5 seconds has elapsed, a horizontal bar chart is displayed. The data can be reset when the dashboard page is refreshed.

## Getting Started

To run this application, you need to start both the frontend and backend servers.

### Backend

1. Go to the backend directory.
2. Install the dependencies with `npm install`.
3. Start the server with `npm start`.

### Frontend

1. Go to the frontend directory.
2. Install the dependencies with `npm install`.
3. Start the server with `npm run dev`.

## Testing

This application includes unit tests for backend.

### Backend

Run the tests with `npm test`.

## Built With

- Frontend: ReactJS, Next.js, Typescript, Material-UI, recharts
- Backend: Node.JS with Typescript, Nest.js, Socket.io