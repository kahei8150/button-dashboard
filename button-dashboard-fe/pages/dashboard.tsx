import { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  CartesianGrid,
  LabelList,
} from 'recharts';
import { Box } from '@mui/material';

// Define the type for our state
type ClicksType = {
  orange: number;
  blue: number;
} | null;

export default function DashboardPage() {
  // Initialize state with null and define its type
  const [clicks, setClicks] = useState<ClicksType>(null);

  // When component mounts and updates
  useEffect(() => {
    // Check if running in a browser environment
    if (typeof window !== 'undefined') {
      // Create a socket connection
      const socket = io('http://localhost:3001');

      // Emit reset event when the component is mounted
      socket.emit('resetData');

      // When 'clicks' event is received, update state
      socket.on('clicks', (newClicks) => {
        setClicks(newClicks);
      });

      // Cleanup function to disconnect when component unmounts
      return () => {
        socket.disconnect();
      };
    }
  }, []); // Run only once after initial render

  // If clicks data is not loaded yet, show a loading message
  if (!clicks) {
    return <div>Loading...</div>;
  }

  const data = [
    {
      name: 'Orange',
      clicks: clicks.orange,
      fill: '#FFA500',
    },
    {
      name: 'Blue',
      clicks: clicks.blue,
      fill: '#6BBCE0',
    },
  ];

  // Determine the maximum number of clicks
  const maxClicks = Math.max(clicks.orange, clicks.blue);

  // Generate an array of numbers from 0 to maxClicks
  const yAxisTicks = Array.from({ length: maxClicks + 1 }, (_, i) => i);

  const boxStyle = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100vh',
  };

  const barChartStyle = { fontFamily: 'Tahoma', margin: '50px' };

  return (
    <Box sx={boxStyle}>
      <BarChart width={600} height={300} data={data} style={barChartStyle}>
        <CartesianGrid />
        <XAxis dataKey="name" />
        <YAxis ticks={yAxisTicks} domain={[0, 'auto']} />
        <Tooltip />
        <Bar dataKey="clicks" barSize={100} />
      </BarChart>
    </Box>
  );
}
