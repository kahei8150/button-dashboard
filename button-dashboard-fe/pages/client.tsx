import { Button } from '@mui/material';
import { Box } from '@mui/system';

export default function ClientPage() {
  // Define a click handler that sends a POST request to a specific color's URL
  const handleClick = async (color: 'orange' | 'blue') => {
    try {
      // Send the POST request
      const response = await fetch(`http://localhost:3000/clicks/${color}`, {
        method: 'POST',
      });

      // If the response was not ok, throw an error
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
    } catch (error) {
      // Log any errors to the console
      console.error('Fetch error:', error);
    }
  };

  const boxStyle = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100vh',
  };

  const buttonStyle = {
    width: '100px',
    height: '100px',
    marginRight: '20px',
    color: 'white',
    fontFamily: 'tahoma',
    textTransform: 'none',
    fontSize: '16px',
    boxShadow: '2px 2px 4px rgba(0, 0, 0, 0.1)',
    borderRadius: '10px',
    transition: '0.3s',
  };

  return (
    <Box sx={boxStyle}>
      <Button
        onClick={() => handleClick('orange')}
        sx={{
          ...buttonStyle,
          backgroundColor: '#FFA500',
          '&:hover': {
            backgroundColor: '#FF8C00',
          },
        }}
      >
        Orange
      </Button>
      <Button
        onClick={() => handleClick('blue')}
        sx={{
          ...buttonStyle,
          backgroundColor: '#6BBCE0',
          '&:hover': {
            backgroundColor: '#4DA9BF',
          },
        }}
      >
        Blue
      </Button>
    </Box>
  );
}
