import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClicksModule } from './clicks/clicks.module';

@Module({
  imports: [ClicksModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
