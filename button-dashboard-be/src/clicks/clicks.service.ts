import { Injectable } from '@nestjs/common';

@Injectable()
export class ClicksService {
  private readonly clicks = { orange: 0, blue: 0 };

  getClicks() {
    return this.clicks; // Return the current click counts
  }

  incrementClicks(color: 'orange' | 'blue') {
    this.clicks[color]++; // Increment the click count for a color
  }

  resetClicks() {
    this.clicks.orange = 0;
    this.clicks.blue = 0;
  }
}