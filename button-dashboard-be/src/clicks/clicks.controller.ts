import { Controller, Get, Post, Param } from '@nestjs/common';
import { ClicksService } from './clicks.service';
import { ClicksGateway } from './clicks.gateway';

@Controller('clicks')
export class ClicksController {
  constructor(
    private clicksService: ClicksService,
    private clicksGateway: ClicksGateway,
  ) {}

  @Get()
  getClicks() {
    return this.clicksService.getClicks(); // Return the current click counts
  }

  @Post(':color')
  incrementClicks(@Param('color') color: 'orange' | 'blue') {
    this.clicksService.incrementClicks(color); // Increment the click count for a color
    this.clicksGateway.broadcastClicks(); // Broadcast the updated click counts
  }
}
