import { Module } from '@nestjs/common';
import { ClicksController } from './clicks.controller';
import { ClicksService } from './clicks.service';
import { ClicksGateway } from './clicks.gateway';

@Module({
  controllers: [ClicksController],
  providers: [ClicksService, ClicksGateway],
})
export class ClicksModule {}
