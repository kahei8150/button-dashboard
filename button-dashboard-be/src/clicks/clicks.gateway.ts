import { WebSocketGateway, WebSocketServer, SubscribeMessage } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ClicksService } from './clicks.service';

@WebSocketGateway(3001, {
  cors: { origin: 'http://localhost:8080' },
})
export class ClicksGateway {
  @WebSocketServer()
  server: Server;

  constructor(private clicksService: ClicksService) {}

  handleConnection(client: any, ...args: any[]) {
    client.emit('clicks', this.clicksService.getClicks());
  }

  // Listen for 'resetData' event from client
  @SubscribeMessage('resetData')
  handleResetData(client: Socket) {
    // Reset the clicks data
    this.clicksService.resetClicks();

    // Broadcast the reset click counts
    this.broadcastClicks();
  }

  broadcastClicks() {
    this.server.emit('clicks', this.clicksService.getClicks()); // Broadcast the updated click counts
  }
}